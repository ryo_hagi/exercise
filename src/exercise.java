import java.util.Scanner;
public class exercise {
    static void order (String food){
        System.out.println("You have ordered "+food+". Thank you!");

    }
    public static void main(String[] args) {
        String food[]={"Tempura","Ramen","Udon"};
        System.out.println("What would you like to order:");
        for(int i=0; i<food.length; i++) {
            System.out.println(i + 1 + ". " + food[i]);
        }
        System.out.print("Your order [1-3]:");
        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        order(food[number-1]);
    }
}
